.. _gsoc-idea-beaglebone-audio-applications:

Beagle Bone Audio Applications
###############################

.. card:: 
    
    :fas:`volume-high;pst-color-secondary` **Porting multi-channel sound card drivers to recent kernels**
    ^^^^^

    Goal: Porting sound card drivers to recent kernels.

    As part of this project you will develop and/or update kernel audio drivers for some soundcards 
    currently available for the BeagleBoard series of SBCs. The objective is to have support for the 
    latest TI kernel (> 5.10) and for some of the most recent TI SoC, such as the Sitara AM6xxx series 
    that is on the `SK-AM62 <https://www.ti.com/tool/SK-AM62>`_ board and the TDA4VM that is on the 
    `BeagleBone AI-64 <https://beagleboard.org/ai-64>`_.

    1. The Bela and BelaMini capes feature a TLV320AIC3104 codec (stereo I/O) controlled via I2C and connected to the McASP clock and data lines. This could be used in older kernels and architectures (e.g.: up to 4.14 on Sitara AM3358) with the simple-audio-card driver and a relevant device tree fragment. It has to be tested whether it still works on more recent kernels and SoCs.
    2. A multi-channel version (BelaMiniMultichannelExpander) combines 1x TLV320AIC3104 and 3xTLV320AIC3106 to achieve 8in/8out; this is supported via a `Bela-specific library <https://github.com/BelaPlatform/Bela/blob/master/core/I2c_MultiTLVCodec.cpp>`_ but there is no kernel support for it yet.
    3. The Bela cape Rev C adds an ES9080Q 8-channel DAC which is also connected to the McASP; this is supported via a `user-space Bela-specific library <https://github.com/BelaPlatform/Bela/blob/master/core/Es9080_Codec.cpp>`_, but there is no kernel support for it yet.
    4. The CTAG capes have an AD1938 codec (4in / 8out) which is controlled via SPI, has an on-board crystal oscillator and are connected to the McASP data and clock lines. This has its own device driver which has been fixed up a few times over time (most recently `here <https://forum.bela.io/d/2578-ctag-face-support-in-linux-5-x-kernel/18>`_, AFAIK) but its features are limited and needs to be tidied up and tested on newer kernels.

    - **Software Skills:** C, Linux, Kernel
    - **Possible Mentors:** rma, henrix, giuliomoro, jluthra
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** where available, listed above
    - **References:** 
        - https://github.com/BelaPlatform/Bela
        - https://learn.bela.io/products/

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`volume-high;pst-color-secondary` **IP Media Streaming**
    ^^^^

    - **Goal:** Building on a Beagle GSoC project of 2017 and 2020, the idea is to extend / add audio network streaming capabilities using the AVB and / or the Ravenna / AES67 standards.
    - **Software Skills:** C/C++, Linux, Kernel
    - **Possible Mentors:** rma, henrix
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** TBD
    - **References:** 
        - https://niklaswan.github.io/GSoC-Overview 
        - https://elinux.org/BeagleBoard/GSoC/BeagleBoneAVB

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`volume-high;pst-color-secondary` **Bela audio driver for common audio APIs**
    ^^^^

    Bela uses its own custom audio API which bypasses ALSA altogether. When adding support for Supercollider and Csound, 
    we had to add a new audio driver to these pieces of software to call into Bela’s C API. In order to make the process 
    of adding Bela support to more programming languages in the future easier, we could think of simply adding Bela support 
    to some common audio backend libraries, so that a single effort can be reused across several pieces of software that 
    use that same library. Examples of such libraries are: portaudio, rtaudio and jack and there may be more. In this 
    project you’ll write a Bela driver for one or more of these libraries and build at least one application that uses 
    the library (e.g.: ChucK, which uses RtAudio or Pure Data, which uses portaudio). You will need a good knowledge of C 
    and build systems. Upon successful completion, the project will make it easier to add more applications and programming languages to Bela.

    - **Goal:** Improve the portability of Bela code across audio APIs/ Hardware Skills: Minimal
    - **Software Skills:** C / make Possible Mentors: giuliomoro
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** 
        - https://github.com/BelaPlatform/Bela
        - https://github.com/thestk/rtaudio
        - https://github.com/PortAudio/portaudio

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 

.. card:: 

    :fas:`volume-high;pst-color-secondary` **Live patching of Pure Data on embedded devices**
    ^^^^

    Pure Data (Pd) is a visual data-flow language for audio applications and it is one of the several languages supported 
    by Bela. Pd ships as a cross-platform software with a C backend and a Tcl/Tk GUI (replaced by HTML5 in the PurrData project). 
    When running Pd on Bela, the user can leverage the analog and digital I/Os of the board to integrate audio processing with 
    the physical connectivity capabilities of the board. Bela provides access to a browser-based IDE for programming the board 
    but, since a desktop environment is not available, the current workflow for using Pure Data on Bela consists in editing the 
    patch on the host computer and then copying it over to Bela where it gets executed.

    One of the limitations of this two-step workflow is that the immediateness of the live-patching behaviour of Pd is lost. In this project, 
    the student will leverage the fact that Pd communicates with its GUI via a socket to run the GUI on the host computer while running the 
    audio backend on Bela. This requires a number of adaptations to Pd, including:

    - managing the GUI startup process to allow GUI and audio backend to be started separately and connect to each other
    - allowing access to paths on the remote device
    - loading patches through the GUI instead of from the backend
    - delegating some of the functionalities currently handled in the backend to the GUI

    Knowledge of C is required as part of the work will take place in the audio backend, however there will be need to operate on the 
    GUI side as well. A good starting point for the project would be last year's GSoC project which managed to run PurrData entirely 
    in the browser. At the end of a successful project we should be able to submit a pull request in the upstream Pure Data or Purr 
    Data repositories. The benefits of this contribution will not be exclusive to Bela, but can benefit the Pd community at large, 
    making it easier to use Pure Data on embedded devices.

    - **Goal:** Allow live patching of Pd on embedded devices. Hardware Skills: Minimal
    - **Software Skills:** Javascript/HTML, possibly using Svelte as a framework
    - **Possible Mentors:** giuliomoro
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** 
        - http://github.com/pure-data/pure-data/
        - https://git.purrdata.net/jwilkes/purr-data/
        - https://gist.github.com/hugonxc/75e0bb838b1953cfa52fa87045bf5b98#file-gsoc2020_hugocarvalho_purrdata-md

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size` 